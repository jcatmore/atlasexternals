# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration for building GoogleTest/GoogleMock as part of the
# release build.
#

# The name of the package:
atlas_subdir( GoogleTest )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The build needs python:
if( ATLAS_BUILD_PYTHON )
   set( _extraArgs
      -DCMAKE_PREFIX_PATH:STRING=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   # In case we are picking up Python from LCG, make sure that the GoogleTest
   # build finds it. Note that if we pick up Python simply from the system,
   # the build will find it without any extra settings.
   find_package( PythonInterp 2.7 REQUIRED )
   find_package( PythonLibs 2.7 REQUIRED )
   set( _extraArgs -DCMAKE_PREFIX_PATH:STRING=${PYTHON_ROOT} )
endif()

# Extra option(s) for the configuration:
set( _extraOptions )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} )
endif()

# Temporary build directory to use:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GoogleTestBuild )

# Build the package for the build area:
ExternalProject_Add( GoogleTest
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY https://github.com/google/googletest.git
   GIT_TAG ff07a5d
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DBUILD_GTEST:BOOL=ON
   -DBUILD_GMOCK:BOOL=ON
   -DBUILD_SHARED_LIBS:BOOL=ON
   ${_extraArgs}
   CMAKE_ARGS ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( GoogleTest buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing GoogleTest into the build area"
   DEPENDEES install )
add_dependencies( Package_GoogleTest GoogleTest )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( GoogleTest Python )
endif()

# Install GoogleTest/GoogleMock:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install the CMake module(s) from the package:
install( FILES cmake/FindGMock.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
