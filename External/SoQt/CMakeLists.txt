# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name:
atlas_subdir( SoQt )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# External package(s):
find_package( Qt5 REQUIRED )
find_package( Coin3D REQUIRED )

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SoQtBuild )

# Extra configuration parameters:
set( _extraConf )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   set( _extraConf -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Set up the build of SoQt:
ExternalProject_Add( SoQt
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY https://github.com/ric-bianchi/SoQt.git
   GIT_TAG 9af2071
   CMAKE_CACHE_ARGS -DCMAKE_PREFIX_PATH:PATH=${QT5_ROOT};${COIN3D_ROOT}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir} ${_extraConf}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( SoQt buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing SoQt into the build area"
   DEPENDEES install )
add_dependencies( Package_SoQt SoQt )

# Install SoQt:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
