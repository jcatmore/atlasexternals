# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration building APE as part of the release build.
# To be kept in sync with the requirements file of the package.
#

# Set the name of the package:
atlas_subdir( APE )

# Set up its package dependencies:
atlas_depends_on_subdirs(
   PRIVATE Externals/yampl )

# External dependencies:
find_package( TBB )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Get the offline repository location, if it's set in the environment:
if( NOT "$ENV{SVNROOT}" STREQUAL "" )
   set( _svnroot $ENV{SVNROOT} )
else()
   set( _svnroot "svn+ssh://svn.cern.ch/reps/atlasoff" )
endif()

# Directory to put temporary build results into:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/APEBuild )

# Build the package for the build area:
ExternalProject_Add( APE
   PREFIX ${CMAKE_BINARY_DIR}
   SVN_REPOSITORY ${_svnroot}/Offloading/APE/tags/APE-01-02-00
   CMAKE_CACHE_ARGS
   -DTBB_INSTALL_DIR:PATH=${TBB_INSTALL_DIR}
   -DYAMPL_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DBOOST_ROOT:PATH=${BOOST_ROOT}
   -DBOOST_INCLUDEDIR:PATH=${BOOST_INCLUDEDIR}
   -DCMAKE_PREFIX_PATH:PATH=${YAMLCPP_ROOT}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_SYSTEM_IGNORE_PATH:STRING=/usr/include;/usr/lib;/usr/lib32;/usr/lib64
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( APE buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory ${_buildDir}/lib/cmake
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing APE into the build area"
   DEPENDEES install )
add_dependencies( APE yampl )
add_dependencies( Package_APE APE )

# Install APE:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
