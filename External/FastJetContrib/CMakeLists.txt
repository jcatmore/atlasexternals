# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building the FastJet contrib libraries as part of the offline
# software build.
#

# Set the package name:
atlas_subdir( FastJetContrib )

# Set the package's dependencies:
atlas_depends_on_subdirs( PUBLIC External/FastJet )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetContribBuild )

# Set up the build of FastJetContrib for the build area:
ExternalProject_Add( FastJetContrib
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.026.tar.gz
   URL_MD5 cdef3a0e3a510b89d88bda4acbe5b199
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND ./configure
   --fastjet-config=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/fastjet-config
   --prefix=${_buildDir} CXXFLAGS=-fPIC
   BUILD_IN_SOURCE 1
   BUILD_COMMAND make
   COMMAND make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   )
add_dependencies( FastJetContrib FastJet )
add_dependencies( Package_FastJetContrib FastJetContrib )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
